package com.example.demo.controller;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CoreDataRestObject implements Serializable {
	private static final long serialVersionUID = 7912978854227932100L;

	private Long ecId;
	private String nominative;
	private String personTypeCode;
	private String ucId;
	private Integer nominativeQi;
	private Integer personTypeCodeQi;
	private Integer ucIdQi;
	private BigDecimal completenessIndex;
	private BigDecimal qualityIndex;
	private Long version;
	private Long createDate;
	private Long updateDate;
	private String username;
	private Long freshnessDate;
}
