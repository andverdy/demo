package com.example.demo.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class Prova2Controller {

    private final String TOKEN = "TOKEN";

    @GetMapping("/getProva2")
    public String getProva2(@RequestParam Integer token) {
        String nuovo = "nuovo";
		String vecchio = "vecchio";
        return this.TOKEN + token + nuovo + vecchio;
    }

}

